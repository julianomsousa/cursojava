package br.com.classejava.exemplo.testeinterface;

public interface InterfaceTeste {
	
	public void metodoA();
	public String metodoB();
	public Integer metodoC();

}

package br.com.classejava.exemplo.classe.abstrata.exemplo;

public abstract class Animal {

	public abstract void move(Float distance);
}

package br.com.classejava.exemplo.classe;

public class ExemploClasseComAtributos {

	String cadeiaDeCaracteres;
	
	public Integer umNumero;
	
	private long umNumeroLong;
	
	public static void main(String[] args) {
		ExemploClasseComAtributos exemploClasseComAtributos = new ExemploClasseComAtributos();
		exemploClasseComAtributos.umNumeroLong = 1l;
	}

}

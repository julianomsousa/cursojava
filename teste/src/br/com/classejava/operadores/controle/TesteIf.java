package br.com.classejava.operadores.controle;

public class TesteIf {
	
	public static void main(String[] args) {
		
		boolean variavelVerificacao = false;
		
		if (variavelVerificacao){
			System.out.println("Resultado: " + variavelVerificacao);
		} else if (!variavelVerificacao){ 
			System.out.println("Resultado else if: " + variavelVerificacao);
		} else{
			System.out.println("Resultado Else" + variavelVerificacao);
		}
		
	}

}

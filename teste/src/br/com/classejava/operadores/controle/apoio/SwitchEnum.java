package br.com.classejava.operadores.controle.apoio;

public enum SwitchEnum {
	
	CASE_1(1),
	CASE_2(2),
	CASE_3(3),
	CASE_4(4),
	CASE_5(5);

	private Integer valor;

	SwitchEnum(Integer valor) {
		this.valor = valor;
	}

	public Integer getValor() {
		return valor;
	}

	
}

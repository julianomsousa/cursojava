package br.com.classejava.operadores.controle;
import br.com.classejava.operadores.controle.apoio.*;
public class TesteSwitchCase {
	
	/**
	 * Metodo para switch case
	 * 
	 * @param enumeravel
	 * @return o valor final
	 */
	
	private static Integer getEnumValue(SwitchEnum enumeravel){
		Integer retorno = null;
	
	
	switch(enumeravel){
	case CASE_1:
		retorno = enumeravel.getValor() * 40;
		break;
	case CASE_2:
		retorno = enumeravel.getValor() * 30;
		break;
	case CASE_3:
		retorno = enumeravel.getValor() * 20;
		break;
	case CASE_4:
		retorno = enumeravel.getValor() * 10;
		break;
	default:
		retorno = enumeravel.getValor() *1;
		break;
	}
	return retorno;
	}
	public static void main(String[] args){
		System.out.println(getEnumValue(SwitchEnum.CASE_1));
		System.out.println(getEnumValue(SwitchEnum.CASE_2));
		System.out.println(getEnumValue(SwitchEnum.CASE_3));
		System.out.println(getEnumValue(SwitchEnum.CASE_4));
		System.out.println(getEnumValue(SwitchEnum.CASE_5));
	}
}

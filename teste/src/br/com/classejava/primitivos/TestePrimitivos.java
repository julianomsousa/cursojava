package br.com.classejava.primitivos;

public class TestePrimitivos {
	
	public static void main(String[] args) {
		
		Long wrapperLong = 20l;
		System.out.println(wrapperLong);
		
		Integer wrapperInteiro = 2;
		System.out.println(wrapperInteiro.getClass());
		
		String[] arrayStrings = new String[10];
		arrayStrings[0] = "Curso Java";
//		System.out.println(arrayStrings.toString());
	    
		boolean testeIgual = 2 == 3;
		boolean testeDiferente = 2 != 3;
		boolean testeMenor = 2 < 3;
		boolean testeMenorIgual = 2 <=2;
		boolean testeMaior = 2 > 3;
		boolean testeMaiorIgual = 2 >= 3;
		
		System.out.println(testeIgual);
		System.out.println(testeDiferente);
		System.out.println(testeMenor);
		System.out.println(testeMenorIgual);
		System.out.println(testeMaior);
		System.out.println(testeMaiorIgual);
		
		String stringDiferente = "diferente";
		String stringIgual = "diferente";
		TesteReferencia igual = new TesteReferencia();
		igual.conteudo = "1";
		TesteReferencia diferente = new TesteReferencia();
		diferente.conteudo = "1";		
		
		boolean testeIgualRefe = igual == diferente;
		System.out.println(testeIgualRefe);
		boolean testeIgualObjetos = stringDiferente == stringIgual;
		System.out.println(testeIgualObjetos);
	}

}

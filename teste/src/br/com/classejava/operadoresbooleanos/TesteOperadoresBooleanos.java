package br.com.classejava.operadoresbooleanos;

public class TesteOperadoresBooleanos {
	
	public static void main(String[] args) {
		
		boolean x , z, resultado;
		x = true;
		z = true;
		int a = 0;
		int b = 1; 
		
		resultado = x && z;
		System.out.println(resultado);
		
		resultado = x || z;
		System.out.println(resultado);
		
		resultado = x = !z;
		System.out.println(resultado);
		
		System.out.println(a == b && ++a <= b);
		System.out.println(a == b & ++a <= b);
		
		a = 0;
		b = 1;
		System.out.println(a == --b | ++a <= b);
		
		
	}

}
